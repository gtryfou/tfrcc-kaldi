/* 
 * File:   feature-tfrcc.h
 * Author: tryfou
 *
 * Created on November 16, 2015, 2:11 PM
 */

#ifndef KALDI_FEAT_FEATURE_TFRCC_H_
#define KALDI_FEAT_FEATURE_TFRCC_H_

#include <map>
#include <string>

#include "feat/feature-functions.h"

namespace kaldi {
/// @addtogroup  feat FeatureExtraction
/// @{


/// TfrccOptions contains basic options for computing TFRCC features
/// It only includes things that can be done in a "stateless" way, i.e.
/// it does not include energy max-normalization.
/// It does not include delta computation.
struct TfrccOptions {
  FrameExtractionOptions frame_opts;
  TimeFreqReassignementOptions tfr_opts;
  MelBanksOptions mel_opts;
  int32 num_ceps;  // e.g. 13: num cepstral coeffs, counting zero.
  bool use_energy;  // use energy; else C0
  BaseFloat energy_floor;
  bool raw_energy;  // If true, compute energy before preemphasis and windowing
  BaseFloat cepstral_lifter;  // Scaling factor on cepstra for HTK compatibility.
                              // if 0.0, no liftering is done.
  bool htk_compat;  // if true, put energy/C0 last and introduce a factor of
                    // sqrt(2) on C0 to be the same as HTK.
  bool debug_tfrcc;
  bool use_dominance_spectrogram;
  
  TfrccOptions() : mel_opts(23),
                  // defaults the #mel-banks to 23 for the TFRCC computations.
                  // this seems to be common for 16khz-sampled data,
                  // but for 8khz-sampled data, 15 may be better.
                  num_ceps(13),
                  use_energy(true),
                  energy_floor(0.0),  // not in log scale: a small value e.g. 1.0e-10
                  raw_energy(true),
                  cepstral_lifter(22.0),
                  htk_compat(false),
                  debug_tfrcc(false),
                  use_dominance_spectrogram(false)
 {}
  
  void Register(OptionsItf *opts) {
    frame_opts.Register(opts);
    mel_opts.Register(opts);
    tfr_opts.Register(opts);
    opts->Register("num-ceps", &num_ceps,
                   "Number of cepstra in TFRCC computation (including C0)");
    opts->Register("use-energy", &use_energy,
                   "Use energy (not C0) in TFRCC computation");
    opts->Register("energy-floor", &energy_floor,
                   "Floor on energy (absolute, not relative) in TFRCC computation");
    opts->Register("raw-energy", &raw_energy,
                   "If true, compute energy before preemphasis and windowing");
    opts->Register("cepstral-lifter", &cepstral_lifter,
                   "Constant that controls scaling of TFRCCs");
    opts->Register("htk-compat", &htk_compat,
                   "If true, put energy or C0 last and use a factor of sqrt(2) on "
                   "C0.  Warning: not sufficient to get HTK compatible features "
                   "(need to change other parameters).");
    opts->Register("debug-tfrcc", &debug_tfrcc,
                   "Print out debugging information for reassigned spectrogram computation");
    opts->Register("use-dominance", &use_dominance_spectrogram,
                   "Use dominance weighting for feature extraction");
  }
};

class MelBanks;


/// Class for computing TfrCC features; see \ref feat_tfrcc for more information.
class Tfrcc {
 public:
  explicit Tfrcc(const TfrccOptions &opts);
  ~Tfrcc();

  int32 Dim() const { return opts_.num_ceps; }

  /// Will throw exception on failure (e.g. if file too short for even one
  /// frame).  The output "wave_remainder" is the last frame or two of the
  /// waveform that it would be necessary to include in the next call to Compute
  /// for the same utterance.  It is not exactly the un-processed part (it may
  /// have been partly processed), it's the start of the next window that we
  /// have not already processed.
  void Compute(const VectorBase<BaseFloat> &wave,
               BaseFloat vtln_warp,
               Matrix<BaseFloat> *output,
               Vector<BaseFloat> *wave_remainder = NULL);

  /// Const version of Compute()
  void Compute(const VectorBase<BaseFloat> &wave,
               BaseFloat vtln_warp,
               Matrix<BaseFloat> *output,
               Vector<BaseFloat> *wave_remainder = NULL) const;
  
  typedef TfrccOptions Options;
 private:
  void ComputeInternal(const VectorBase<BaseFloat> &wave,
                       const MelBanks &mel_banks,
                       Matrix<BaseFloat> *output,
                       Vector<BaseFloat> *wave_remainder = NULL) const;
  
  const MelBanks *GetMelBanks(BaseFloat vtln_warp);

  const MelBanks *GetMelBanks(BaseFloat vtln_warp,
                              bool *must_delete) const;
  
  TfrccOptions opts_;
  Vector<BaseFloat> lifter_coeffs_;
  Matrix<BaseFloat> dct_matrix_;  // matrix we left-multiply by to perform DCT.
  BaseFloat log_energy_floor_;
  std::map<BaseFloat, MelBanks*> mel_banks_;  // BaseFloat is VTLN coefficient.
  FeatureWindowFunction feature_window_function_;
  FeatureWindowFunction target_feature_window_function_;
  Vector<BaseFloat> window_time_derivative_, window_time_ramp_, window_deriv_ramp_, target_window_;
  SplitRadixRealFft<BaseFloat> *srfft_;
  TimeFrequencyReassignement *tfr_;
  bool debug_;
  
  KALDI_DISALLOW_COPY_AND_ASSIGN(Tfrcc);
};


/// @} End of "addtogroup feat"
}  // namespace kaldi


#endif  // KALDI_FEAT_FEATURE_TFRCC_H_
