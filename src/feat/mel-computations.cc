// feat/mel-computations.cc

// Copyright 2009-2011  Phonexia s.r.o.;  Karel Vesely;  Microsoft Corporation

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <algorithm>
#include <iostream>

#include "feat/mel-computations.h"
#include "feat/feature-functions.h"

namespace kaldi {

    MelBanks::MelBanks(const MelBanksOptions &opts,
            const FrameExtractionOptions &frame_opts,
            BaseFloat vtln_warp_factor) :
    htk_mode_(opts.htk_mode), sample_freq_(frame_opts.samp_freq) {
        int32 num_bins = opts.num_bins;
        if (num_bins < 3) KALDI_ERR << "Must have at least 3 mel bins";
        // BaseFloat sample_freq = frame_opts.samp_freq;
        int32 window_length = static_cast<int32> (frame_opts.samp_freq * 0.001 * frame_opts.frame_length_ms);
        window_length_padded_ =
                (frame_opts.round_to_power_of_two ?
                RoundUpToNearestPowerOfTwo(window_length) :
                window_length);
        KALDI_ASSERT(window_length_padded_ % 2 == 0);
        int32 num_fft_bins = window_length_padded_ / 2;
        fft_bin_freqs.Resize(num_fft_bins);
        
        BaseFloat nyquist = 0.5 * sample_freq_;

        BaseFloat low_freq = opts.low_freq, high_freq;
        if (opts.high_freq > 0.0)
            high_freq = opts.high_freq;
        else
            high_freq = nyquist + opts.high_freq;

        if (low_freq < 0.0 || low_freq >= nyquist
                || high_freq <= 0.0 || high_freq > nyquist
                || high_freq <= low_freq)
            KALDI_ERR << "Bad values in options: low-freq " << low_freq
                << " and high-freq " << high_freq << " vs. nyquist "
                << nyquist;

        BaseFloat fft_bin_width = sample_freq_ / window_length_padded_;
        // fft-bin width [think of it as Nyquist-freq / half-window-length]

        BaseFloat mel_low_freq = MelScale(low_freq);
        BaseFloat mel_high_freq = MelScale(high_freq);

        debug_ = opts.debug_mel;

        // divide by num_bins+1 in next line because of end-effects where the bins
        // spread out to the sides.
        BaseFloat mel_freq_delta = (mel_high_freq - mel_low_freq) / (num_bins + 1);

        BaseFloat vtln_low = opts.vtln_low,
                vtln_high = opts.vtln_high;
        if (vtln_high < 0.0) vtln_high += nyquist;

        if (vtln_warp_factor != 1.0 &&
                (vtln_low < 0.0 || vtln_low <= low_freq
                || vtln_low >= high_freq
                || vtln_high <= 0.0 || vtln_high >= high_freq
                || vtln_high <= vtln_low))
            KALDI_ERR << "Bad values in options: vtln-low " << vtln_low
                << " and vtln-high " << vtln_high << ", versus "
                << "low-freq " << low_freq << " and high-freq "
                << high_freq;

        bins_.resize(num_bins);
        center_freqs_.Resize(num_bins);
        center_freqs_mel.Resize(num_bins);
        left_freqs_mel.Resize(num_bins);
        right_freqs_mel.Resize(num_bins);

        for (int32 bin = 0; bin < num_bins; bin++) {
            BaseFloat left_mel = mel_low_freq + bin * mel_freq_delta,
                    center_mel = mel_low_freq + (bin + 1) * mel_freq_delta,
                    right_mel = mel_low_freq + (bin + 2) * mel_freq_delta;

            if (vtln_warp_factor != 1.0) {
                left_mel = VtlnWarpMelFreq(vtln_low, vtln_high, low_freq, high_freq,
                        vtln_warp_factor, left_mel);
                center_mel = VtlnWarpMelFreq(vtln_low, vtln_high, low_freq, high_freq,
                        vtln_warp_factor, center_mel);
                right_mel = VtlnWarpMelFreq(vtln_low, vtln_high, low_freq, high_freq,
                        vtln_warp_factor, right_mel);
            }
            center_freqs_(bin) = InverseMelScale(center_mel);
            center_freqs_mel(bin) = center_mel;
            left_freqs_mel(bin) = left_mel;
            right_freqs_mel(bin) = right_mel;
            // this_bin will be a vector of coefficients that is only
            // nonzero where this mel bin is active.
            Vector<BaseFloat> this_bin(num_fft_bins);
            int32 first_index = -1, last_index = -1;
            for (int32 i = 0; i < num_fft_bins; i++) {
                BaseFloat freq = (fft_bin_width * i); // center freq of this fft bin.
                fft_bin_freqs(i) = freq;
                 
                BaseFloat mel = MelScale(freq);
                if (mel > left_mel && mel < right_mel) {
                    BaseFloat weight;
                    if (mel <= center_mel)
                        weight = (mel - left_mel) / (center_mel - left_mel);
                    else
                        weight = (right_mel - mel) / (right_mel - center_mel);
                    this_bin(i) = weight;
                    if (first_index == -1)
                        first_index = i;
                    last_index = i;
                }
            }
            KALDI_ASSERT(first_index != -1 && last_index >= first_index
                    && "You may have set --num-mel-bins too large.");

            bins_[bin].first = first_index;
            int32 size = last_index + 1 - first_index;
            bins_[bin].second.Resize(size);
            bins_[bin].second.CopyFromVec(this_bin.Range(first_index, size));

            // Replicate a bug in HTK, for testing purposes.
            if (opts.htk_mode && bin == 0 && mel_low_freq != 0.0)
                bins_[bin].second(0) = 0.0;

        }
        if (debug_) {
            KALDI_LOG << "Left, center, right frequencies (mel)";
            for (size_t i = 0; i < bins_.size(); i++) {
                KALDI_LOG << left_freqs_mel(i) << ", " << center_freqs_mel(i) << ", " << right_freqs_mel(i) << "\n";
            }
            KALDI_LOG << "Left, center, right frequencies (Hz)";
            for (size_t i = 0; i < bins_.size(); i++) {
                KALDI_LOG << InverseMelScale(left_freqs_mel(i)) << ", " << center_freqs_(i) << ", " << InverseMelScale(right_freqs_mel(i)) << "\n";
            }
        }
    }

    BaseFloat MelBanks::VtlnWarpFreq(BaseFloat vtln_low_cutoff, // upper+lower frequency cutoffs for VTLN.
            BaseFloat vtln_high_cutoff,
            BaseFloat low_freq, // upper+lower frequency cutoffs in mel computation
            BaseFloat high_freq,
            BaseFloat vtln_warp_factor,
            BaseFloat freq) {
        /// This computes a VTLN warping function that is not the same as HTK's one,
        /// but has similar inputs (this function has the advantage of never producing
        /// empty bins).

        /// This function computes a warp function F(freq), defined between low_freq and
        /// high_freq inclusive, with the following properties:
        ///  F(low_freq) == low_freq
        ///  F(high_freq) == high_freq
        /// The function is continuous and piecewise linear with two inflection
        ///   points.
        /// The lower inflection point (measured in terms of the unwarped
        ///  frequency) is at frequency l, determined as described below.
        /// The higher inflection point is at a frequency h, determined as
        ///   described below.
        /// If l <= f <= h, then F(f) = f/vtln_warp_factor.
        /// If the higher inflection point (measured in terms of the unwarped
        ///   frequency) is at h, then max(h, F(h)) == vtln_high_cutoff.
        ///   Since (by the last point) F(h) == h/vtln_warp_factor, then
        ///   max(h, h/vtln_warp_factor) == vtln_high_cutoff, so
        ///   h = vtln_high_cutoff / max(1, 1/vtln_warp_factor).
        ///     = vtln_high_cutoff * min(1, vtln_warp_factor).
        /// If the lower inflection point (measured in terms of the unwarped
        ///   frequency) is at l, then min(l, F(l)) == vtln_low_cutoff
        ///   This implies that l = vtln_low_cutoff / min(1, 1/vtln_warp_factor)
        ///                       = vtln_low_cutoff * max(1, vtln_warp_factor)


        if (freq < low_freq || freq > high_freq) return freq; // in case this gets called
        // for out-of-range frequencies, just return the freq.

        KALDI_ASSERT(vtln_low_cutoff > low_freq &&
                "be sure to set the --vtln-low option higher than --low-freq");
        KALDI_ASSERT(vtln_high_cutoff < high_freq &&
                "be sure to set the --vtln-high option lower than --high-freq [or negative]");
        BaseFloat one = 1.0;
        BaseFloat l = vtln_low_cutoff * std::max(one, vtln_warp_factor);
        BaseFloat h = vtln_high_cutoff * std::min(one, vtln_warp_factor);
        BaseFloat scale = 1.0 / vtln_warp_factor;
        BaseFloat Fl = scale * l; // F(l);
        BaseFloat Fh = scale * h; // F(h);
        KALDI_ASSERT(l > low_freq && h < high_freq);
        // slope of left part of the 3-piece linear function
        BaseFloat scale_left = (Fl - low_freq) / (l - low_freq);
        // [slope of center part is just "scale"]

        // slope of right part of the 3-piece linear function
        BaseFloat scale_right = (high_freq - Fh) / (high_freq - h);

        if (freq < l) {
            return low_freq + scale_left * (freq - low_freq);
        } else if (freq < h) {
            return scale * freq;
        } else { // freq >= h
            return high_freq + scale_right * (freq - high_freq);
        }
    }

    BaseFloat MelBanks::VtlnWarpMelFreq(BaseFloat vtln_low_cutoff, // upper+lower frequency cutoffs for VTLN.
            BaseFloat vtln_high_cutoff,
            BaseFloat low_freq, // upper+lower frequency cutoffs in mel computation
            BaseFloat high_freq,
            BaseFloat vtln_warp_factor,
            BaseFloat mel_freq) {
        return MelScale(VtlnWarpFreq(vtln_low_cutoff, vtln_high_cutoff,
                low_freq, high_freq,
                vtln_warp_factor, InverseMelScale(mel_freq)));
    }


    // "power_spectrum" contains fft energies.

    void MelBanks::Compute(const VectorBase<BaseFloat> &power_spectrum,
            Vector<BaseFloat> *mel_energies_out) const {
        int32 num_bins = bins_.size();
        if (mel_energies_out->Dim() != num_bins)
            mel_energies_out->Resize(num_bins);

        for (int32 i = 0; i < num_bins; i++) {
            int32 offset = bins_[i].first;
            const Vector<BaseFloat> &v(bins_[i].second);
            BaseFloat energy = VecVec(v, power_spectrum.Range(offset, v.Dim()));
            // HTK-like flooring- for testing purposes (we prefer dither)
            if (htk_mode_ && energy < 1.0) energy = 1.0;
            (*mel_energies_out)(i) = energy;

            // The following assert was added due to a problem with OpenBlas that
            // we had at one point (it was a bug in that library).  Just to detect
            // it early.
            KALDI_ASSERT(!KALDI_ISNAN((*mel_energies_out)(i)));
        }

        if (debug_) {
            fprintf(stderr, "MEL BANKS:\n");
            for (int32 i = 0; i < num_bins; i++)
                fprintf(stderr, " %f", (*mel_energies_out)(i));
            fprintf(stderr, "\n");
        }
    }
   
    void MelBanks::ComputeReassigned(const Matrix<BaseFloat> &fft_energies,
            const Matrix<BaseFloat> &time_corrections,
            const Matrix<BaseFloat> &frequency_corrections,
            const Matrix<BaseFloat> &is_pruned,
            const BaseFloat time_step,
            Matrix<BaseFloat> *mel_energies_out) const {


        int32 num_bins = time_corrections.NumCols();
        int32 num_frames = time_corrections.NumRows();
        int32 num_chans = bins_.size();

        Vector<BaseFloat> Tn(num_frames);
        Vector<BaseFloat> Fn(num_bins);

        mel_energies_out->Set(0.0);

        for (int32 n = 0; n < num_frames; n++) {
            Tn(n) = n*time_step;
        }
       

        for (int32 frame_index = 0; frame_index < num_frames; frame_index++) {
            for (int32 bin = 0; bin < num_bins; bin++) {
                if (is_pruned(frame_index, bin) == 1) {
                    if (debug_) {
                        std::cout << "Pruning TFR point.." << std::endl;
                    }
                    continue;
                }

                int32 t_right, t_left, band_left, band_right;
                BaseFloat wt_left, wt_right, w_left, w_right;

                BaseFloat mag = fft_energies(frame_index, bin);
                BaseFloat time = time_corrections(frame_index, bin) + Tn(frame_index);

                if (time < 0 || time > Tn(num_frames-1))
                    continue;

                BaseFloat frequency = frequency_corrections(frame_index, bin) + fft_bin_freqs(bin);
                BaseFloat freq_mel = MelScale(frequency);

                if (freq_mel < left_freqs_mel(0) || freq_mel > right_freqs_mel(num_chans - 1))
                    continue;

                t_right = (int32) (time / time_step) + 1;
                t_left = t_right - 1;
                if (t_left < 0 || t_right >= Tn.Dim())
                    continue;

                wt_right = (time - Tn(t_left)) / time_step;
                wt_left = 1 - wt_right;

                if (freq_mel < center_freqs_mel(0)) {
                    band_right = 0;
                } else {
                    for (int32 band = 1; band < num_chans; band++) {
                        if (freq_mel > center_freqs_mel(band - 1) && freq_mel < center_freqs_mel(band)) {
                            band_right = band;
                            break;
                        }
                    }
                    if (freq_mel > center_freqs_mel(num_chans - 1)) {
                        band_right = num_chans;
                    }
                }
                band_left = band_right - 1;

                if (band_left >= 0 && band_left < num_chans) { // down slope
                    w_left = (right_freqs_mel(band_left) - freq_mel) / (right_freqs_mel(band_left) - center_freqs_mel(band_left));
                    if (w_left > 0) {
                        (*mel_energies_out)(t_left, band_left) += (wt_left * w_left * mag);
                        (*mel_energies_out)(t_right, band_left) += (wt_right * w_left * mag);
                    }
                    if (debug_) std::cout << "band left: " << band_left << " and weight " << w_left << "\n";
                }

                if (band_right >= 0 && band_right < num_chans) { //up slope
                    w_right = (freq_mel - left_freqs_mel(band_right)) / (center_freqs_mel(band_right) - left_freqs_mel(band_right));
                    if (w_right > 0) {
                        (*mel_energies_out)(t_left, band_right) += (wt_left * w_right * mag);
                        (*mel_energies_out)(t_right, band_right) += (wt_right * w_right * mag);
                    }
                    if (debug_) std::cout << "band right: " << band_right << " and weight " << w_right << "\n";
                }
            }
        }
    }

    void MelBanks::ComputeReassigned2(const Matrix<BaseFloat> &fft_energies,
            const Matrix<BaseFloat> &time_corrections,
            const Matrix<BaseFloat> &frequency_corrections,
            const Matrix<BaseFloat> &is_pruned,
            const Vector<BaseFloat> &target_window,
            const FrameExtractionOptions fr_opts,
            Matrix<BaseFloat> *mel_energies_out) const {

        int32 num_bins = time_corrections.NumCols();

        KALDI_ASSERT(num_bins != center_freqs_mel.Dim());
        
        int32 num_frames = time_corrections.NumRows();
        int32 num_chans = bins_.size();
     
        mel_energies_out->Set(0.0);
        
        BaseFloat time_step = fr_opts.frame_shift_ms * 0.001;
        Vector<BaseFloat> frame_centers(num_frames);
        for (int32 n = 0; n < num_frames; n++)            
            frame_centers(n) = time_step * (n + 0.5);
        
          
        int32 tgt_num_frames = mel_energies_out->NumRows();
                
        Vector<BaseFloat> tgt_frames_end(tgt_num_frames);
        Vector<BaseFloat> tgt_frames_start(tgt_num_frames);
       
        for (int32 n = 0; n < tgt_num_frames; n++) {
            tgt_frames_start(n) = frame_centers(n) - 0.5 * fr_opts.frame_length_ms * 0.001; //n * fr_opts.frame_shift_ms * 0.001;
            tgt_frames_end(n) = tgt_frames_start(n) + fr_opts.frame_length_ms * 0.001;
        }
       if(debug_) {
            std::cout << "Target frame start" << std::endl;
            for (int32 n = 0; n < num_frames; n++)            
                std::cout << tgt_frames_start(n) << " " ;
       }
        
        for (int32 frame_index = 0; frame_index < num_frames; frame_index++) {
            for (int32 bin = 0; bin < num_bins; bin++) {
                if (is_pruned(frame_index, bin) == 1) {
                    continue;
                }

                int32 band_left, band_right;
                BaseFloat t_weight, w_left, w_right;
                BaseFloat mag = fft_energies(frame_index, bin);
                
                BaseFloat time = time_corrections(frame_index, bin) + frame_centers(frame_index);
                if (time < 0 || time > tgt_frames_end(tgt_num_frames - 1))
                    continue;
                
                BaseFloat frequency = frequency_corrections(frame_index, bin) + fft_bin_freqs(bin);
                BaseFloat freq_mel = MelScale(frequency);
                
                if (freq_mel < left_freqs_mel(0) || freq_mel > right_freqs_mel(num_chans - 1))
                    continue;
            
                band_right = -1;
                if (freq_mel < center_freqs_mel(0) && freq_mel > left_freqs_mel(0)) {
                    band_right = 0;
                } else {
                    for (int32 band = 1; band < num_chans; band++) {
                        if (freq_mel > left_freqs_mel(band) && freq_mel < center_freqs_mel(band)) {
                            band_right = band;
                            break;
                        }
                    }
                }
                band_left = band_right - 1;
                
                w_left = (right_freqs_mel(band_left) - freq_mel) / (right_freqs_mel(band_left) - center_freqs_mel(band_left));
                w_right = (freq_mel - left_freqs_mel(band_right)) / (center_freqs_mel(band_right) - left_freqs_mel(band_right));
                
                int32 num_tgt_frames = 0;
                std::vector<int32>inds;
                std::vector<BaseFloat>t_weights;
                int32 i=0;
                
            //    if(debug_) {
            //        if(band_left >= 0)
            //            std::cout<< "freq: " << freq_mel <<" goes to (" << left_freqs_mel(band_left)  << ", " << right_freqs_mel(band_left) << ")\n";
            //        if(band_right>=0 && band_right < num_chans)
            //            std::cout<< "\t\t and to (" << left_freqs_mel(band_right)  << ", " << right_freqs_mel(band_right) << ")\n";
                   
              //  }
                //if(debug_) 
                //    std::cout<< "curr time: " << time << " mapped at: ";
               
                for (int32 n = 0; n < tgt_num_frames; n++) {
                    if (time > tgt_frames_start(n) && time <= tgt_frames_end(n)) {
                        num_tgt_frames += 1;
                        i = (time - tgt_frames_start(n)) * sample_freq_ ; 
                        
                 //       if(debug_)  std::cout << tgt_frames_start(n) << " ";
                        t_weight = target_window(i);
                        t_weights.push_back(t_weight);
                        inds.push_back(n);
                    }
                }
              
              //  if(debug_) std::cout << "\n";
                for (int32 m = 0; m < num_tgt_frames; m++) {
                    int32 n = inds.at(m);
                    t_weight = t_weights.at(m) / num_tgt_frames;
                    if (band_left >= 0 && band_left < num_chans) { // down slope
                        (*mel_energies_out)(n, band_left) += (t_weight * w_left * mag);
                    }
                    if (band_right >= 0 && band_right < num_chans) { //up slope
                        (*mel_energies_out)(n, band_right) += (t_weight * w_right * mag);
                    }
                }
            }
        }
    }

    

    void ComputeLifterCoeffs(BaseFloat Q, VectorBase<BaseFloat> *coeffs) {
        // Compute liftering coefficients (scaling on cepstral coeffs)
        // coeffs are numbered slightly differently from HTK: the zeroth
        // index is C0, which is not affected.

        for (int32 i = 0; i < coeffs->Dim(); i++)
            (*coeffs)(i) = 1.0 + 0.5 * Q * sin(M_PI * i / Q);
    }


    // Durbin's recursion - converts autocorrelation coefficients to the LPC
    // pTmp - temporal place [n]
    // pAC - autocorrelation coefficients [n + 1]
    // pLP - linear prediction coefficients [n] (predicted_sn = sum_1^P{a[i] * s[n-i]}})
    //       F(z) = 1 / (1 - A(z)), 1 is not stored in the demoninator

    BaseFloat Durbin(int n, const BaseFloat *pAC, BaseFloat *pLP, BaseFloat *pTmp) {
        BaseFloat ki; // reflection coefficient
        int i;
        int j;

        BaseFloat E = pAC[0];

        for (i = 0; i < n; i++) {
            // next reflection coefficient
            ki = pAC[i + 1];
            for (j = 0; j < i; j++)
                ki += pLP[j] * pAC[i - j];
            ki = ki / E;

            // new error
            BaseFloat c = 1 - ki * ki;
            if (c < 1.0e-5) // remove NaNs for constan signal
                c = 1.0e-5;
            E *= c;

            // new LP coefficients
            pTmp[i] = -ki;
            for (j = 0; j < i; j++)
                pTmp[j] = pLP[j] - ki * pLP[i - j - 1];

            for (j = 0; j <= i; j++)
                pLP[j] = pTmp[j];
        }

        return E;
    }

    void Lpc2Cepstrum(int n, const BaseFloat *pLPC, BaseFloat *pCepst) {
        for (int32 i = 0; i < n; i++) {
            double sum = 0.0;
            int j;
            for (j = 0; j < i; j++) {
                sum += static_cast<BaseFloat> (i - j) * pLPC[j] * pCepst[i - j - 1];
            }
            pCepst[i] = -pLPC[i] - sum / static_cast<BaseFloat> (i + 1);
        }
    }


} // namespace kaldi
