/* 
 * File:   feature-tfrcc.h
 * Author: tryfou
 *
 * Created on November 16, 2015, 2:11 PM
 */
// limitations under the License.


#include "feature-tfrcc.h"

namespace kaldi {

    Tfrcc::Tfrcc(const TfrccOptions &opts)
    : opts_(opts), feature_window_function_(opts.frame_opts), target_feature_window_function_(opts.tfr_opts.tgt_frame_opts_),
    srfft_(NULL), tfr_(NULL) {

        debug_ = opts.debug_tfrcc;

        int32 num_bins = opts.mel_opts.num_bins;
        Matrix<BaseFloat> dct_matrix(num_bins, num_bins);
        ComputeDctMatrix(&dct_matrix);
        // Note that we include zeroth dct in either case.  If using the
        // energy we replace this with the energy. This means a different
        // ordering of features than HTK.
        SubMatrix<BaseFloat> dct_rows(dct_matrix, 0, opts.num_ceps, 0, num_bins);
        dct_matrix_.Resize(opts.num_ceps, num_bins);
        dct_matrix_.CopyFromMat(dct_rows); // subset of rows.
        if (opts.cepstral_lifter != 0.0) {
            lifter_coeffs_.Resize(opts.num_ceps);
            ComputeLifterCoeffs(opts.cepstral_lifter, &lifter_coeffs_);
        }
        if (opts.energy_floor > 0.0)
            log_energy_floor_ = Log(opts.energy_floor);

        int32 padded_window_size = opts.frame_opts.PaddedWindowSize();
        if ((padded_window_size & (padded_window_size - 1)) == 0) // Is a power of two...
            srfft_ = new SplitRadixRealFft<BaseFloat>(padded_window_size);

        // We'll definitely need the filterbanks info for VTLN warping factor 1.0.
        // [note: this call caches it.]  The reason we call this here is to
        // improve the efficiency of the "const" version of Compute().
        GetMelBanks(1.0);

        // Create the time ramp and time derivative functions needed for the RS
        window_time_derivative_.Resize(feature_window_function_.window.Dim());
        ComputeTimeDerivative(opts.frame_opts, feature_window_function_, &window_time_derivative_);

        window_time_ramp_.Resize(feature_window_function_.window.Dim());
        ComputeTimeRamp(opts.frame_opts, feature_window_function_, &window_time_ramp_);

        FeatureWindowFunction tmp_win(opts.frame_opts);
        tmp_win.window = window_time_derivative_;
        window_deriv_ramp_.Resize(feature_window_function_.window.Dim());
        ComputeTimeRamp(opts.frame_opts, tmp_win, &window_deriv_ramp_);

        tfr_ = new TimeFrequencyReassignement(opts.tfr_opts);
    }

    Tfrcc::~Tfrcc() {
        for (std::map<BaseFloat, MelBanks*>::iterator iter = mel_banks_.begin();
                iter != mel_banks_.end();
                ++iter)
            delete iter->second;
        delete srfft_;
    }

    const MelBanks *Tfrcc::GetMelBanks(BaseFloat vtln_warp) {
        MelBanks *this_mel_banks = NULL;
        std::map<BaseFloat, MelBanks*>::iterator iter = mel_banks_.find(vtln_warp);
        if (iter == mel_banks_.end()) {
            this_mel_banks = new MelBanks(opts_.mel_opts,
                    opts_.frame_opts, //the analysis frame options should go here
                    vtln_warp);
            mel_banks_[vtln_warp] = this_mel_banks;
        } else {
            this_mel_banks = iter->second;
        }
        return this_mel_banks;
    }

    const MelBanks *Tfrcc::GetMelBanks(BaseFloat vtln_warp, bool *must_delete) const {
        MelBanks *this_mel_banks = NULL;
        std::map<BaseFloat, MelBanks*>::const_iterator iter =
                mel_banks_.find(vtln_warp);
        if (iter == mel_banks_.end()) {
            this_mel_banks = new MelBanks(opts_.mel_opts,
                    opts_.frame_opts, //the analysis frame options should go here
                    vtln_warp);
            *must_delete = true;
        } else {
            this_mel_banks = iter->second;
            *must_delete = false;
        }
        return this_mel_banks;
    }

    void Tfrcc::Compute(const VectorBase<BaseFloat> &wave,
            BaseFloat vtln_warp,
            Matrix<BaseFloat> *output,
            Vector<BaseFloat> *wave_remainder) {
        const MelBanks *this_mel_banks = GetMelBanks(vtln_warp);
        ComputeInternal(wave, *this_mel_banks, output, wave_remainder);
    }

    void Tfrcc::Compute(const VectorBase<BaseFloat> &wave,
            BaseFloat vtln_warp,
            Matrix<BaseFloat> *output,
            Vector<BaseFloat> *wave_remainder) const {
        bool must_delete_mel_banks;
        const MelBanks *mel_banks = GetMelBanks(vtln_warp,
                &must_delete_mel_banks);

        ComputeInternal(wave, *mel_banks, output, wave_remainder);

        if (must_delete_mel_banks)
            delete mel_banks;
    }

    void Tfrcc::ComputeInternal(const VectorBase<BaseFloat> &wave,
            const MelBanks &mel_banks,
            Matrix<BaseFloat> *output,
            Vector<BaseFloat> *wave_remainder) const {
        KALDI_ASSERT(output != NULL);
        int32 rows_out = NumFrames(wave.Dim(), opts_.frame_opts),
                cols_out = opts_.num_ceps;
        int32 mel_rows_out = NumFrames(wave.Dim(), opts_.tfr_opts.tgt_frame_opts_);

        if (rows_out == 0) {
            output->Resize(0, 0);
            if (wave_remainder != NULL)
                *wave_remainder = wave;
            return;
        }
        int32 number_of_bins = opts_.frame_opts.PaddedWindowSize() / 2;

        output->Resize(mel_rows_out, cols_out);
        Matrix<BaseFloat> time_corrections(rows_out, number_of_bins),
                frequency_corrections(rows_out, number_of_bins),
                power_spectrogram(rows_out, number_of_bins),
                dominance_spectrogram(rows_out, number_of_bins),
                mixed_partial_derivatives(rows_out, number_of_bins);
        Matrix<BaseFloat> is_tfr_pruned(rows_out, number_of_bins);

        if (wave_remainder != NULL)
            ExtractWaveformRemainder(wave, opts_.frame_opts, wave_remainder);

        Vector<BaseFloat> window, time_ramp_window, time_deriv_window, deriv_ramp_window; // windowed waveform.

        std::vector<BaseFloat> temp_buffer; // used by srfft.
        for (int32 r = 0; r < rows_out; r++) { // r is frame index..
            ExtractWindow(wave, r, opts_.frame_opts, feature_window_function_, &window, NULL);
            //(opts_.use_energy && opts_.raw_energy ? &log_energy : NULL));
            FeatureWindowFunction tmp_window(opts_.frame_opts);
            tmp_window.window.CopyFromVec(window_time_derivative_);
            ExtractWindow(wave, r, opts_.frame_opts, tmp_window, &time_deriv_window, NULL);
            tmp_window.window.CopyFromVec(window_time_ramp_);
            ExtractWindow(wave, r, opts_.frame_opts, tmp_window, &time_ramp_window, NULL);
            tmp_window.window.CopyFromVec(window_deriv_ramp_);
            ExtractWindow(wave, r, opts_.frame_opts, tmp_window, &deriv_ramp_window, NULL);

            if (srfft_ != NULL) {// Compute FFT using the split-radix algorithm.
                srfft_->Compute(window.Data(), true, &temp_buffer);
                srfft_->Compute(time_deriv_window.Data(), true, &temp_buffer);
                srfft_->Compute(time_ramp_window.Data(), true, &temp_buffer);
            } else { // An alternative algorithm that works for non-powers-of-two.
                RealFft(&window, true);
                RealFft(&time_deriv_window, true);
                RealFft(&time_ramp_window, true);
                RealFft(&deriv_ramp_window, true);
            }

            Vector<BaseFloat> time_correction, frequency_correction, mixed_partial_derivative;
            Vector<BaseFloat> is_pruned;

            /*  tfr_->Process(window, time_ramp_window, time_deriv_window, 
                           &time_correction, &frequency_correction);
   
              tfr_->ProcessMPD(window, time_ramp_window, time_deriv_window, 
                      deriv_ramp_window, &is_pruned, &mixed_partial_derivative);
             */

            tfr_->Process2(window, time_ramp_window, time_deriv_window, deriv_ramp_window,
                    &time_correction, &frequency_correction, &is_pruned, &mixed_partial_derivative);

            time_corrections.CopyRowFromVec(time_correction, r);
            frequency_corrections.CopyRowFromVec(frequency_correction, r);
            is_tfr_pruned.CopyRowFromVec(is_pruned, r);
            mixed_partial_derivatives.CopyRowFromVec(mixed_partial_derivative, r);
            ComputePowerSpectrum(&window);
            SubVector<BaseFloat> power_spectrum(window, 0, number_of_bins);
            power_spectrogram.CopyRowFromVec(power_spectrum, r);

            for (int c = 0; c < power_spectrum.Dim(); c++) {
                BaseFloat curr_value = frequency_correction(c);
                if (curr_value != 0) {
                    curr_value = 1 / curr_value;
                    dominance_spectrogram(r, c) = std::abs(power_spectrum(c) * curr_value);
                }
                else{
                    dominance_spectrogram(r, c) = power_spectrum(c);
                }       
       //         KALDI_ASSERT(!KALDI_ISFINITE(dominance_spectrogram(r, c)));
            }
        }
        if (debug_) {
            int32 counter = 0;
            for (int32 n1 = 0; n1 < is_tfr_pruned.NumRows(); n1++)
                for (int32 n2 = 0; n2 < is_tfr_pruned.NumCols(); n2++) {
                    if (is_tfr_pruned(n1, n2) == 1)
                        counter += 1;
                }
            int32 total = is_tfr_pruned.NumRows() * is_tfr_pruned.NumCols();
            std::cout << "Pruned " << counter << " points out of " << total << " points.\n";
        }
        Matrix<BaseFloat> mel_energies_m(mel_rows_out, opts_.mel_opts.num_bins);

    //     mel_banks.ComputeReassigned(power_spectrogram, time_corrections, 
    //        frequency_corrections, is_tfr_pruned,
    //         opts_.frame_opts.frame_shift_ms * 0.001, &mel_energies_m);

        //std::cout << "Compute new rabiwindow method... " << std::endl;
        if (debug_) {
            std::cout << "Spectrogram size: " << rows_out << std::endl;
            std::cout << "New mel size: " << mel_rows_out << std::endl;
        }
        if (opts_.mel_opts.compute_reassigned) {
            if (!opts_.use_dominance_spectrogram) {
                
                mel_banks.ComputeReassigned2(power_spectrogram, time_corrections,
                        frequency_corrections, is_tfr_pruned,
                        target_feature_window_function_.window, opts_.tfr_opts.tgt_frame_opts_,
                        &mel_energies_m);
            } else {
                mel_banks.ComputeReassigned2(dominance_spectrogram, time_corrections,
                        frequency_corrections, is_tfr_pruned,
                        target_feature_window_function_.window, opts_.tfr_opts.tgt_frame_opts_,
                        &mel_energies_m);
            }
        } else {
            for (int32 r = 0; r < rows_out; r++) {
                Vector<BaseFloat> mel_energies;
                if (opts_.use_dominance_spectrogram) {
                    mel_banks.Compute(dominance_spectrogram.Row(r), &mel_energies);
                } else {
                     mel_banks.Compute(power_spectrogram.Row(r), &mel_energies);
                }
                mel_energies_m.CopyRowFromVec(mel_energies, r);
            }
        }


        // std::cout << "New Rabiwindow method COMPLETED" << std::endl;

        if (debug_) {
            WriteKaldiObject(time_corrections, "time_corrections.txt", false);
            WriteKaldiObject(frequency_corrections, "frequency_corrections.txt", false);
            WriteKaldiObject(power_spectrogram, "power_spectrogram.txt", false);
            WriteKaldiObject(mixed_partial_derivatives, "mixed_partial_derivatives.txt", false);
            WriteKaldiObject(is_tfr_pruned, "pruning.txt", false);
            WriteKaldiObject(mel_energies_m, "mel.txt", false);
            WriteKaldiObject(dominance_spectrogram, "dominance_spectrogram.txt", false);
        }

        for (int32 r = 0; r < mel_rows_out; r++) {

            BaseFloat log_energy;
            ExtractWindow(wave, r, opts_.tfr_opts.tgt_frame_opts_, target_feature_window_function_, &window,
                    (opts_.use_energy && opts_.raw_energy ? &log_energy : NULL));
            if (opts_.use_energy && !opts_.raw_energy)
                log_energy = Log(std::max(VecVec(window, window),
                    std::numeric_limits<BaseFloat>::min()));

            SubVector<BaseFloat> mel_energy = mel_energies_m.Row(r);
            // avoid log of zero (which should be prevented anyway by dithering).

            mel_energy.ApplyFloor(std::numeric_limits<BaseFloat>::min());
            mel_energy.ApplyLog(); // take the log.

            SubVector<BaseFloat> this_tfrcc(output->Row(r));

            // this_tfrcc = dct_matrix_ * mel_energies [which now have log]
            this_tfrcc.AddMatVec(1.0, dct_matrix_, kNoTrans, mel_energy, 0.0);

            if (opts_.cepstral_lifter != 0.0)
                this_tfrcc.MulElements(lifter_coeffs_);

            if (opts_.use_energy) {
                if (opts_.energy_floor > 0.0 && log_energy < log_energy_floor_)
                    log_energy = log_energy_floor_;
                this_tfrcc(0) = log_energy;
            }

            if (opts_.htk_compat) {
                BaseFloat energy = this_tfrcc(0);
                for (int32 i = 0; i < opts_.num_ceps - 1; i++)
                    this_tfrcc(i) = this_tfrcc(i + 1);
                if (!opts_.use_energy)
                    energy *= M_SQRT2; // scale on C0 (actually removing scale
                // we previously added that's part of one common definition of
                // cosine transform.)
                this_tfrcc(opts_.num_ceps - 1) = energy;
            }
        }

    }






} // namespace kaldi
