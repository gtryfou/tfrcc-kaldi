[![Build Status](https://travis-ci.org/kaldi-asr/kaldi.svg?branch=master)]
(https://travis-ci.org/kaldi-asr/kaldi)

Kaldi Speech Recognition Toolkit
================================

To build the toolkit: see `./INSTALL`.  These instructions are valid for UNIX
systems including various flavors of Linux; Darwin; and Cygwin (has not been
tested on more "exotic" varieties of UNIX).  For Windows installation
instructions (excluding Cygwin), see `windows/INSTALL`.

To run the example system builds, see `egs/README.txt`

If you encounter problems (and you probably will), please do not hesitate to
contact the developers (see below). In addition to specific questions, please
let us know if there are specific aspects of the project that you feel could be
improved, that you find confusing, etc., and which missing features you most
wish it had.

Kaldi information channels
--------------------------

For HOT news about Kaldi see [the project site](http://kaldi-asr.org/).

[Documentation of Kaldi](http://kaldi-asr.org/doc/):
- Info about the project, description of techniques, tutorial for C++ coding.
- Doxygen reference of the C++ code.

[Kaldi forums and mailing lists](http://kaldi-asr.org/forums.html):
- User list kaldi-help:
    [Web interface/archive](https://groups.google.com/forum/#!forum/kaldi-help) ||
    [Subscribe] (mailto:kaldi-help+subscribe@googlegroups.com) ||
    [Post] (mailto:kaldi-help@googlegroups.com)
- Developer list kaldi-developers:
    [Web interface/archive](https://groups.google.com/forum/#!forum/kaldi-developers) ||
    [Subscribe] (mailto:kaldi-developers+subscribe@googlegroups.com) ||
    [Post] (mailto:kaldi-developers@googlegroups.com)
- Also try luck and search in [SourceForge archives](https://sourceforge.net/p/kaldi/discussion/).

TFRCC feature extraction
------------------------

The TFRCC extraction is performed with changes in the following list of files
src/feat/feature-functions.h
src/feat/feature-functions.cc
src/feat/feature-tfrcc.h
src/feat/feature-tfrcc.cc
src/feat/feature-tfrcc-test.cc
src/feat/mel-computations.h
src/feat/mel-computations.cc

src/featbin/compute-tfrcc-feats.cc 
src/featbin/compute-tfrcc-feats.h



